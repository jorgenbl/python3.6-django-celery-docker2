# Docker file for automatic build of docker container #

This is a dockerfile for automatic build of container based on "official" python3.6 with the following software:

Latest:
Django
Redis
Celery
ciscoconfparse
ciscoparse
node 7
netmiko
python git
butterfly shell
django-filebrowser
django-grapelli
openssh
MongoDB
vis.js
Jedha
ansible
ntc-ansible
napalm
django-netjsongraph
